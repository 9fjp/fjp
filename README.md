# Překladař C-like jazyka pro PL/0
Cílem této práce je vytvořit překldač pro jednoduchý jazyk se syntaxí inspirovanou jazykem C pro architekturu PL/0.

| Člen týmu | Email |
| --- | --- |
| Vašta Jakub | vastja@students.zcu.cz |
| Hlaváč Petr| petrh@students.zcu.cz |

## Zadání

| Základní | 1 bodové | 2 bodové | 3 bodové | Celkem/Minimum|
| --- | --- | --- | --- | --- |
| 10/10 | 7/10 | 6/18 | 0/18 | **23/20** |

### Základní

| Funcionalita | syntax |
| --- | --- |
| definice celočíselných proměnných | `int` |
| definice celočíselných konstant | `const`|
| přiřazení | `=` |
| základní aritmetiku a logiku | `+, -, *, /, and , or , !, (), <, >, ==, <>` |
| cyklus | `while` |
| jednoduchou podmínku | `if(condition) {}` |
| definice podprogramu | `return_val func_name(params) {}` |

### Jednoduchá

| Funcionalita | syntax |
| --- | --- |
| další typ | `for` |
| else větev | `else` |
| datový typ boolean a logické operace s ním | `bool` |
| datový typ real (s celočíselnými instrukcemi) | |
| datový typ string (s operátory pro spojování řětezců) | |
| rozvětvená podmínka |`switch` |
| příkazy pro vstup a výstup | `read, write` |

### Složitější

| Funcionalita | syntax |
| --- | --- |
| složený datový typ | `record {basic_datat_types}`|
| pole a práce s jeho prvky | `basic_data_type | record [index]` |
| návratová hodnota podprogramu | return_val func_name(params) {} |

# Gramatika
