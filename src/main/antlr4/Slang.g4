grammar Slang;
@header {
    package generated;
}

// Parser rules

type
    : STRUCT IDENTIFIER
    | simpleType
    ;

simpleType
    : INT
    | REAL
    | CHAR
    | BOOL
    ;

translationUnit
    : variables? functions? mainFunction
    ;


variableDefinition
    : type IDENTIFIER ASSIGN expression
    | type IDENTIFIER
    ;

arrayDefinition
    : type LSBRACKET RSBRACKET IDENTIFIER ASSIGN arrayInitializer
    | type LSBRACKET NUMBER RSBRACKET IDENTIFIER
    | CHAR LSBRACKET RSBRACKET IDENTIFIER ASSIGN STRING_LITERAL
    ;

structDefinition
    : STRUCT IDENTIFIER LCBRACKET (structInternalVariables)+ RCBRACKET
    ;

structInternalVariables
    : (CONST)? variableDefinition SEMICOLON
    | (CONST)? arrayDefinition SEMICOLON
    ;

variable
    : (CONST)? variableDefinition SEMICOLON
    | (CONST)? structDefinition SEMICOLON
    | (CONST)? arrayDefinition SEMICOLON
    ;

variables
    : (variable)+
    ;

simpleExpression
    : access
    | NUMBER
    | REAL_NUMBER
    | CHARACTER
    | TRUE
    | FALSE
    | LBRACKET expression RBRACKET
    | functionCall
    ;

access
    : IDENTIFIER (LSBRACKET expression RSBRACKET)? (POINT access)?
    ;

unaryExpression
    : (unaryOperator)? simpleExpression
    ;

unaryOperator
    : NEGATION
    | MINUS
    ;

multiplicativeExpression
    : unaryExpression
    | unaryExpression me
    ;

me
    : MULTIPLY unaryExpression
    | DIVIDE unaryExpression
    | MODULO unaryExpression
    | MULTIPLY unaryExpression me
    | DIVIDE unaryExpression me
    | MODULO unaryExpression me
    ;

additiveExpression
    : multiplicativeExpression
    | multiplicativeExpression ae
    ;

ae
    : PLUS multiplicativeExpression
    | MINUS multiplicativeExpression
    | PLUS multiplicativeExpression ae
    | MINUS multiplicativeExpression ae
    ;

relationalExpression
    : additiveExpression
    | additiveExpression re
    ;

re
    : LT additiveExpression
    | GT additiveExpression
    | LE additiveExpression
    | GE additiveExpression
    | LT additiveExpression re
    | GT additiveExpression re
    | LE additiveExpression re
    | GE additiveExpression re
    ;

equalityExpression
    : relationalExpression
    | relationalExpression ee
    ;

ee
    : EQ relationalExpression
    | NE relationalExpression
    | EQ relationalExpression ee
    | NE relationalExpression ee
    ;

logicalAndExpression
    : equalityExpression
    | equalityExpression lae
    ;

lae
    : AND equalityExpression
    | AND equalityExpression lae
    ;

expression
    : logicalAndExpression
    | logicalAndExpression e
    ;

e
    : OR logicalAndExpression
    | OR logicalAndExpression e
    ;

arrayInitializer
    : LCBRACKET expression (COMMA expression)* RCBRACKET
    | expression
    ;

functions
    : function+
    ;

function
    : functionReturnValue IDENTIFIER LBRACKET arguments? RBRACKET LCBRACKET body? returnStatement? RCBRACKET (SEMICOLON?)
    ;

functionReturnValue
    : (type | type LSBRACKET NUMBER RSBRACKET | VOID)
    ;

arguments
    :  type (IDENTIFIER | LSBRACKET NUMBER RSBRACKET IDENTIFIER) (COMMA type (IDENTIFIER | LSBRACKET NUMBER RSBRACKET IDENTIFIER) )*
    ;

returnStatement
    : RETURN expression SEMICOLON
    ;

body
    : (commands | variables)+
    | LCBRACKET body RCBRACKET SEMICOLON?
    ;

commands
    : (command)+
    ;

command
    : assignment SEMICOLON
    | variables
    | ifControl
    | switchControl
    | forControl
    | whileControl
    | doWhileControl
    | repeatUntilControl
    | functionCall SEMICOLON
    ;

assignment
    : access ASSIGN expression
    ;

ifControl
    : IF LBRACKET expression RBRACKET LCBRACKET body RCBRACKET (ELIF LBRACKET expression RBRACKET LCBRACKET body RCBRACKET)* (ELSE LCBRACKET body RCBRACKET)?
    ;

switchControl
    : SWITCH LBRACKET expression RBRACKET LCBRACKET (CASE (CHARACTER | NUMBER) COLON body)+ RCBRACKET
    ;

forControl
     : FOR LBRACKET access SEMICOLON expression SEMICOLON expression RBRACKET LCBRACKET body RCBRACKET
    ;

whileControl
    : WHILE LBRACKET expression RBRACKET LCBRACKET body RCBRACKET
    ;

doWhileControl
    : DO LCBRACKET body RCBRACKET WHILE LBRACKET expression RBRACKET
    ;

repeatUntilControl
    : REPEAT LCBRACKET body RCBRACKET UNTIL LBRACKET expression RBRACKET
    ;

functionCall
    : IDENTIFIER LBRACKET (expression ( COMMA expression)*)? RBRACKET
    ;

mainFunction
    : MAIN LBRACKET RBRACKET LCBRACKET body? RCBRACKET (SEMICOLON?)
    ;

// Lexer rules

fragment A
   : ('a' | 'A')
   ;

fragment B
   : ('b' | 'B')
   ;

fragment C
   : ('c' | 'C')
   ;

fragment D
   : ('d' | 'D')
   ;

fragment E
   : ('e' | 'E')
   ;

fragment F
   : ('f' | 'F')
   ;

fragment G
   : ('g' | 'G')
   ;

fragment H
   : ('h' | 'H')
   ;

fragment I
   : ('i' | 'I')
   ;

fragment J
   : ('j' | 'J')
   ;

fragment K
   : ('k' | 'K')
   ;

fragment L
   : ('l' | 'L')
   ;

fragment M
   : ('m' | 'M')
   ;

fragment N
   : ('n' | 'N')
   ;

fragment O
   : ('o' | 'O')
   ;

fragment P
   : ('p' | 'P')
   ;

fragment Q
   : ('q' | 'Q')
   ;

fragment R
   : ('r' | 'R')
   ;

fragment S
   : ('s' | 'S')
   ;

fragment T
   : ('t' | 'T')
   ;

fragment U
   : ('u' | 'U')
   ;

fragment V
   : ('v' | 'V')
   ;

fragment W
   : ('w' | 'W')
   ;

fragment X
   : ('x' | 'X')
   ;

fragment Y
   : ('y' | 'Y')
   ;

fragment Z
   : ('z' | 'Z')
   ;

MAIN : M A I N;

CASE : C A S E;

IF : I F;

ELIF : E L I F;

ELSE : E L S E;

SWITCH : S W I T C H;

FOR : F O R;

WHILE : W H I L E;

SEMICOLON : ';';

COMMA : ',';

POINT : '.';

COLON : ':';

CONST : C O N S T;

VOID : V O I D;

STRUCT : S T R U C T;

INT : I N T;

REAL : R E A L;

CHAR : C H A R;

BOOL : B O O L;

TRUE : T R U E;

FALSE : F A L S E;

STRING_LITERAL : '"'~["\n\r\t]*'"';

ASSIGN : '=';

NEGATION : '!';

NE : '!=';

EQ : '==';

PLUS : '+';

MINUS : '-';

MULTIPLY : '*';

DIVIDE : '/';

MODULO : '%';

LT : '<';

GT : '>';

LE : '<=';

GE : '>=';

OR : '||';

AND : '&&';

LSBRACKET : '[';

RSBRACKET : ']';

LCBRACKET : '{';

RCBRACKET : '}';

LBRACKET : '(';

RBRACKET : ')';

RETURN : R E T U R N;

DO : D O;

REPEAT : R E P E A T;

UNTIL : U N T I L;

NUMBER : [0-9]+;

CHARACTER : ('\''~['\n\r\t]'\'')|('\'''\\'[nrt]'\'');

REAL_NUMBER : [0-9]*'.'[0-9]*;

COMMENT : '//' ~('\r' | '\n')*  -> skip;

IDENTIFIER : ('_' | [a-z] | [A-Z])('_' | [0-9] | [a-z] | [A-Z])*;

WHITESPACE : (' ' | '\t' | '\r'? '\n' | '\r')+ -> skip;

