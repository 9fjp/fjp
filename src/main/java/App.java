import compiler.FileGenerator;
import compiler.InstructionList;
import generated.SlangLexer;
import generated.SlangParser;
import org.antlr.v4.runtime.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import semantic.SemanticChecker;
import compiler.CodeGeneratingVisitor;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.util.concurrent.CancellationException;

/**
 * https://tomassetti.me/antlr-mega-tutorial/
 */
public class App {

    private static final Logger logger = LoggerFactory.getLogger(App.class);

    public static void printHelp(){
        String help = "Welcome to our Slang Compiler\nYou can call our program with argument -h to print help\nIf you want to compile your Slang source code put name of your source file to first argument. ";
        System.out.println(help);
    }

    public static void main(String[] args) {
        String fileName;

        if(args.length != 1){
            printHelp();
            return;
        }

        if(args[0].equals("-h")){
            printHelp();
            return;
        }
        else{
            fileName = args[0];
        }

        //InputStream inputStream = App.class.getClassLoader().getResourceAsStream(fileName);

        try {
            //CharStream chs = CharStreams.fromStream(inputStream, Charset.forName("UTF8"));
            CharStream chs = CharStreams.fromPath(Paths.get(fileName),Charset.forName("UTF8"));
            Lexer lexer = new SlangLexer(chs);
            TokenStream ts = new CommonTokenStream(lexer);
            SlangParser parser = new SlangParser(ts);
            // Redundant
            parser.setBuildParseTree(true);

            SlangParser.TranslationUnitContext tuc = parser.translationUnit();
            if (parser.getNumberOfSyntaxErrors() > 0) {
                throw new CancellationException("Parser encounter errors.");
            }

            SemanticChecker visitor = new SemanticChecker();
            // Use getters and setters to obtain tables from semantic checker

            visitor.visit(tuc);

            CodeGeneratingVisitor generator = new CodeGeneratingVisitor(visitor.getSymbolTable(),visitor.getStructuresDefinitions());

            generator.visit(tuc);
            logger.info("BUILD SUCCESS");

        }
        catch(CancellationException e) {
            logger.error("BUILD FAILED");
            return;
        }
        catch(IOException e) {
            logger.error("File with "+ fileName +" not found.");
            return;
        }
        catch(Exception e) {
            logger.error("BUILD FAILED - Internal exception");
            return;
        }

        // For debuging reasons

        //InstructionList.getInstance().printList();

        // For final version
        FileGenerator.setFilePath(Paths.get(fileName));
        FileGenerator generator = FileGenerator.getInstance();
        generator.generateFile(InstructionList.getInstance());

        logger.info("Code generated in folder with compiler. Name: " + FileGenerator.getFileName() );

    }

}
