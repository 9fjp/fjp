package compiler;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;

public class FileGenerator {

    private static Path filePath;
    private static String fileName;
    private final String OUTPUT_FILE_TYPE = ".pl0";

    private static FileGenerator single_instance;

    private FileWriter fw;
    private BufferedWriter bw;

    private FileGenerator(){

        try {
            fileName = filePath.getFileName().toString().split("\\.")[0] + OUTPUT_FILE_TYPE;
            fw = new FileWriter(fileName);
            bw = new BufferedWriter(fw);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static FileGenerator getInstance(){
        if(single_instance == null){
            single_instance = new FileGenerator();
        }
        return single_instance;
    }

    public static void setFilePath(Path filePath) {
        FileGenerator.filePath = filePath;
    }

    public static String getFileName() {
        return fileName;
    }

    public void generateFile(InstructionList instructionList){

        for (PL0Instruction in : instructionList.getInstructList()) {
            try {
                bw.write(in.toString());
                bw.newLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        closeFileGenerator();

    }

    private void closeFileGenerator(){

        try {
            bw.close();
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }



}
