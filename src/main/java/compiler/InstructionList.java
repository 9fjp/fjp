package compiler;

import java.util.LinkedList;
import java.util.List;

public class InstructionList {

    private static InstructionList single_instance;

    private List<PL0Instruction> instructList;

    private InstructionList(){
        this.instructList = new LinkedList<>();
    }

    private boolean debug = true;
    private int debugFrom = 0;
    private int debugTo = 1000;

    public static InstructionList getInstance(){
        if(single_instance == null){
            single_instance = new InstructionList();
        }
        return single_instance;
    }

    public void addInstruction(PL0Instruction instruct){
        this.instructList.add(instruct);
    }

    public void addInstructions(List<PL0Instruction> instructions){
        for (PL0Instruction inst : instructions) {
            addInstruction(inst);
        }
    }

    public List<PL0Instruction> getInstructList() {
        return instructList;
    }

    public void printList(){

        for(PL0Instruction instruct:this.instructList){
            System.out.println(instruct.toString());
            if (debug && instruct.getNumber() >= debugFrom && instruct.getNumber() <= debugTo) {
                System.out.println("&STK");
            }
        }
    }

}
