package compiler;

public enum InstructionType{

    // In case of jump 3 values are stored in stack
    // 0 Return address
    // 1 Dynamic pointer
    // 2 Static pointer

    // Basic PL/0 instructions
    LIT, // LIT 0, A ulož konstantu A do zásobníku
    OPR, // OPR 0, A proveď instrukci A
    LOD, // lod L,A    ulož hodnotu proměnné z adr. L,A na vrchol zásobníku
    STO, // sto L,A    zapiš do proměnné z adr. L,A hodnotu z vrcholu zásobníku
    CAL, // cal L,A    volej proceduru A z úrovně L
    INT, // int 0,A    zvyš obsah top-registru zásobníku o hodnotu A
    JMP, // jmp 0,A    proveď skok na adresu A
    JMC, // jmc 0,A    proveď skok na adresu A, je-li hodnota na vrcholu zásobníku 0
    RET, // ret 0,0    návrat z procedury (return)

    // Extensions of PL/0
    // I/O operations
    REA, // rea 0 0         - read integer to the top of the stack
    WRI, // wri 0 0         - write value from stack top as ascii character
    // Real operations
    OPF, // opf 0 function  - as OPR for real operators
    RTI, // rti 0 0         - from two top integer values one int value
    ITR, // itr 0 0         - from one top integer value two real values
    LDA, // lda 0 0		    - vrchol zasobniku data z adresy uvedene na vrcholu zasobniku */
    STA, // sta 0 0         - na zasobniku hodnota, adresa -> ulozeni hodnoty na adresu */
    PLD, // pld 0 0         - na zasobniku L, A -> na vrchol hodnota z (L, A) */
    PST; // pst 0 0         - na zasobnkku X, L, A -> na adresu (L, A) se ulozi X */

    public static int getStackChange(InstructionType it) {

        switch (it) {
            case LIT:
            case LOD:
            case REA:
                return 1;
            case OPR:
            case STO:
            case WRI:
            case RTI:
            case PLD:
            case JMC:
                return -1;
            case OPF:
                return -2;
            case PST:
                return -3;
            case CAL:
            case JMP:
            case INT:
            case ITR:
            case RET:
                return 0;
            default:
                throw new IllegalArgumentException();
        }

    }

}

