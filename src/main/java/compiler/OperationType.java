package compiler;

public enum OperationType{
    UNARY_MINUS(1),
    PLUS(2),
    MINUS(3),
    MULTIPLY(4),
    DIVIDE(5),
    MODULO(6),
    ODD(7),
    EQUALS(8),
    NOT_EQUALS(9),
    LESS(10),
    GREATER_OR_EQUAL(11),
    GREATER(12),
    LESS_OR_EQUAL(13);

    private Integer hierarchy;

    private OperationType(final Integer hierarchy){
        this.hierarchy = hierarchy;
    }

    public Integer getHierarchy(){
        return hierarchy;
    }

}
