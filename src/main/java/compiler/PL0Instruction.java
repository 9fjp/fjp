package compiler;

public class PL0Instruction {
    private int number;
    private InstructionType instruct;
    private int par1, par2;

    public PL0Instruction(int number,InstructionType instruct, int par1, int par2){
        this.number = number;
        this.instruct = instruct;
        this.par1 = par1;
        this.par2 = par2;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public InstructionType getInstruct() {
        return instruct;
    }

    public void setInstruct(InstructionType instruct) {
        this.instruct = instruct;
    }

    public int getPar1() {
        return par1;
    }

    public void setPar1(int par1) {
        this.par1 = par1;
    }

    public int getPar2() {
        return par2;
    }

    public void setPar2(int par2) {
        this.par2 = par2;
    }

    @Override
    public String toString() {
        return String.format("%d %s %d %d", number, instruct.toString(), par1, par2);
    }

}
