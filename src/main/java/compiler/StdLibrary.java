package compiler;

import com.fasterxml.jackson.databind.BeanProperty;
import descriptors.*;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class StdLibrary {

    private static final int ZERO_CODE = 48;
    private static final int SEPARATOR_CODE = 46;
    private static final int LINE_FEED_CODE = 10;
    private static final int CARRIAGE_RETURN_CODE = 13;

    private static int commandNumber;
//    private static int stackPosition;

    public static int copyAddress = -1;

    private static InstructionList ilist = InstructionList.getInstance();

    private static PL0Instruction getPL0Instruction(InstructionType it, int par1, int par2) {
//        StdLibrary.stackPosition += InstructionType.getStackChange(it);
//        if (it == InstructionType.INT) {
//            stackPosition += par2;
//        }
        return new PL0Instruction(StdLibrary.commandNumber++, it, par1, par2);
    }


    /**Generates STDlib functions
     *
     * @param cV instance of CodeGenerating visitor
     * @param symbolTable symbolTable
     * @param commandNum currentCommandNumber in cV
     * @return new command number to contiue generating in cV
     */
    public static int generateSTD(CodeGeneratingVisitor cV,Scope<Descriptor> symbolTable, int commandNum){
        StdLibrary.commandNumber = commandNum;

        // goes through every STDlib function and if there is a functionCall for it in program than generates code

        FunctionDescriptor func = (FunctionDescriptor) symbolTable.descriptors.get("i2r");
        if(func.isUsed()) {
            func.setAddress(commandNumber);
            generateI2R(func);
        }

        func = (FunctionDescriptor) symbolTable.descriptors.get("r2i");
        if(func.isUsed()) {
            func.setAddress(commandNumber);
            generateR2I(func);
        }

        func = (FunctionDescriptor) symbolTable.descriptors.get("i2b");
        if(func.isUsed()) {
            func.setAddress(commandNumber);
            generateI2B(func);
        }

        func = (FunctionDescriptor) symbolTable.descriptors.get("ch2i");
        if(func.isUsed()) {
            func.setAddress(commandNumber);
            generateCH2I(func);
        }

        boolean universalGenerated = false;
        int universalAddress = -1;

        func = (FunctionDescriptor) symbolTable.descriptors.get("b2i");
        if(func.isUsed()) {
            universalGenerated = true;
            universalAddress = commandNumber;
            func.setAddress(universalAddress);
            generateUniversal(func);
        }

        func = (FunctionDescriptor) symbolTable.descriptors.get("i2ch");
        if(func.isUsed()) {
            if(universalGenerated == false){
                universalAddress = commandNumber;
                generateUniversal(func);
            }
            func.setAddress(universalAddress);
        }

        func = (FunctionDescriptor) symbolTable.descriptors.get("readChar");
        if(func.isUsed()) {
            func.setAddress(commandNumber);
            generateReadChar(func);
        }

        func = (FunctionDescriptor) symbolTable.descriptors.get("writeChar");
        if(func.isUsed()) {
            func.setAddress(commandNumber);
            generateWriteChar(func);
        }

        func = (FunctionDescriptor) symbolTable.descriptors.get("s2i");
        if(func.isUsed()) {
            func.setAddress(commandNumber);
            generateS2I(func);
        }

        func = (FunctionDescriptor) symbolTable.descriptors.get("s2r");
        if(func.isUsed()) {
            func.setAddress(commandNumber);
            generateS2R(func);
        }

        func = (FunctionDescriptor) symbolTable.descriptors.get("write");
        if(func.isUsed()) {
            func.setAddress(commandNumber);
            generateWrite(func);
        }

        func = (FunctionDescriptor) symbolTable.descriptors.get("read");
        if(func.isUsed()) {
            func.setAddress(commandNumber);
            generateRead(func);
        }

        // TODO might be offered to user for copying
        generateCopy();

        return commandNumber;
    }

    /** Generates i2r as function
     *
     * @param func
     */
    public static void generateI2R(FunctionDescriptor func){

        ilist.addInstruction(getPL0Instruction(InstructionType.INT,0,3));

        func.getArguments().get(0).setAddress(-1);
        ilist.addInstruction(getPL0Instruction(InstructionType.LOD, 0,-1));

        i2rBody();

        ilist.addInstruction(getPL0Instruction(InstructionType.STO, 0, -2));
        ilist.addInstruction(getPL0Instruction(InstructionType.STO, 0, -3));

        ilist.addInstruction(getPL0Instruction(InstructionType.RET,0,0));
    }

    /** Generates i2b as function
     *
     * @param func
     */
    public static void generateI2B(FunctionDescriptor func){

        ilist.addInstruction(getPL0Instruction(InstructionType.INT,0,3));

        func.getArguments().get(0).setAddress(-1);
        ilist.addInstruction(getPL0Instruction(InstructionType.LOD, 0,-1));

        i2bBody();

        ilist.addInstruction(getPL0Instruction(InstructionType.STO, 0, -2));

        ilist.addInstruction(getPL0Instruction(InstructionType.RET,0,0));
    }

    /**Generates ch2i as function
     *
     * @param func
     */
    public static void generateCH2I(FunctionDescriptor func){

        ilist.addInstruction(getPL0Instruction(InstructionType.INT,0,3));

        func.getArguments().get(0).setAddress(-1);
        ilist.addInstruction(getPL0Instruction(InstructionType.LOD, 0,-1));

        ch2iBody();

        ilist.addInstruction(getPL0Instruction(InstructionType.STO, 0, -2));

        ilist.addInstruction(getPL0Instruction(InstructionType.RET,0,0));
    }

    /**Generates r2i as function
     *
     * @param func
     */
    public static void generateR2I(FunctionDescriptor func){

        ilist.addInstruction(getPL0Instruction(InstructionType.INT,0,3));

        func.getArguments().get(0).setAddress(-2);
        ilist.addInstruction(getPL0Instruction(InstructionType.LOD, 0,-2));
        ilist.addInstruction(getPL0Instruction(InstructionType.LOD, 0,-1));

        r2iBody();

        ilist.addInstruction(getPL0Instruction(InstructionType.STO, 0, -3));

        ilist.addInstruction(getPL0Instruction(InstructionType.RET,0,0));
    }

    /**Generates universal function
     * Universal means that it only loads a value and returns the same value
     * Used for function i2ch b2i
     * @param func
     */
    public static void generateUniversal(FunctionDescriptor func){

        ilist.addInstruction(getPL0Instruction(InstructionType.INT,0,3));

        func.getArguments().get(0).setAddress(-1);
        ilist.addInstruction(getPL0Instruction(InstructionType.LOD, 0,-1));

        ilist.addInstruction(getPL0Instruction(InstructionType.STO, 0, -2));

        ilist.addInstruction(getPL0Instruction(InstructionType.RET,0,0));
    }

    /** Generate ReadChar as Function
     *
     * @param func
     */
    public static void generateReadChar(FunctionDescriptor func){

        ilist.addInstruction(getPL0Instruction(InstructionType.INT,0,3));

        readCharBody();

        ilist.addInstruction(getPL0Instruction(InstructionType.STO, 0, -1));

        ilist.addInstruction(getPL0Instruction(InstructionType.RET,0,0));
    }

    /** Generate WriteChar as function
     *
     * @param func
     */
    public static void generateWriteChar(FunctionDescriptor func){

        ilist.addInstruction(getPL0Instruction(InstructionType.INT,0,3));

        ilist.addInstruction(getPL0Instruction(InstructionType.LOD, 0, -1));

        writeCharBody();

        ilist.addInstruction(getPL0Instruction(InstructionType.RET,0,0));
    }


    /** Generate s2i as function
     *
     * @param func
     */
    public static void generateS2I(FunctionDescriptor func){

        funcInitAndLoadTriple();

        s2i();

        ilist.addInstruction(getPL0Instruction(InstructionType.STO, 0, -4));

        ilist.addInstruction(getPL0Instruction(InstructionType.RET,0,0));
    }

    /** Generate s2r as function
     *
     * @param func
     */
    public static void generateS2R(FunctionDescriptor func){

        funcInitAndLoadTriple();

        s2rBody();

        ilist.addInstruction(getPL0Instruction(InstructionType.STO, 0, -4));
        ilist.addInstruction(getPL0Instruction(InstructionType.STO, 0, -5));

        ilist.addInstruction(getPL0Instruction(InstructionType.RET,0,0));
    }

    /**Generate write as function
     *
     * @param func
     */
    public static void generateWrite(FunctionDescriptor func){

        funcInitAndLoadTriple();

        writeBody();

        ilist.addInstruction(getPL0Instruction(InstructionType.RET,0,0));
    }

    /**Generate read as function
     *
     * @param func
     */
    public static void generateRead(FunctionDescriptor func){

        funcInitAndLoadTriple();
        readBody();

        ilist.addInstruction(getPL0Instruction(InstructionType.STO, 0, -4));
        ilist.addInstruction(getPL0Instruction(InstructionType.RET,0,0));
    }


    /** Generate Copy as function
     *
     */
    private static void generateCopy() {

        copyAddress = StdLibrary.commandNumber;

        ilist.addInstruction(getPL0Instruction(InstructionType.INT, 0, 3));
        // source level
        ilist.addInstruction(getPL0Instruction(InstructionType.LOD, 0, -5));
        // source address
        ilist.addInstruction(getPL0Instruction(InstructionType.LOD, 0, -4));

        // destination level
        ilist.addInstruction(getPL0Instruction(InstructionType.LOD, 0, -3));
        // destination address
        ilist.addInstruction(getPL0Instruction(InstructionType.LOD, 0, -2));

        // counter
        ilist.addInstruction(getPL0Instruction(InstructionType.LOD, 0, -1));

        memCopyBody();

        ilist.addInstruction(getPL0Instruction(InstructionType.RET, 0, 0));

    }

    public static List<PL0Instruction> copy(int commandNumber) {
        List<PL0Instruction> instructions = new LinkedList<>();
        StdLibrary.commandNumber = commandNumber;
        ilist.addInstruction(getPL0Instruction(InstructionType.CAL, 1, copyAddress));
        return instructions;
    }

    private static void funcInitAndLoadTriple() {

        //scope
        ilist.addInstruction(getPL0Instruction(InstructionType.INT,0,3));
        //level
        ilist.addInstruction(getPL0Instruction(InstructionType.LOD,0,-3));
        // address
        ilist.addInstruction(getPL0Instruction(InstructionType.LOD,0,-2));
        // size
        ilist.addInstruction(getPL0Instruction(InstructionType.LOD,0,-1));

    }
    

    private static void i2rBody() {
       
        // Real part is zero
        ilist.addInstruction(getPL0Instruction(InstructionType.LIT, 0, 0));
        // Create int-part and real-part on stack
        ilist.addInstruction(getPL0Instruction(InstructionType.ITR, 0, 0));

    }


    private static void i2bBody() {

        ilist.addInstruction(getPL0Instruction(InstructionType.LOD, 0, 3));
        ilist.addInstruction(getPL0Instruction(InstructionType.LIT, 0, 0));
        ilist.addInstruction(getPL0Instruction(InstructionType.OPR, 0, OperationType.NOT_EQUALS.getHierarchy()));
        // Magic number is commands count till end of the program_print_integer i2b
        ilist.addInstruction(getPL0Instruction(InstructionType.JMC, 0, StdLibrary.commandNumber + 3));
        ilist.addInstruction(getPL0Instruction(InstructionType.LOD, 0, 3));
        ilist.addInstruction(getPL0Instruction(InstructionType.OPR, 0, OperationType.DIVIDE.getHierarchy()));

    }


    private static void ch2iBody() {

        ilist.addInstruction(getPL0Instruction(InstructionType.LIT, 0,  ZERO_CODE));
        ilist.addInstruction(getPL0Instruction(InstructionType.OPR, 0, OperationType.MINUS.getHierarchy()));

    }

    private static void r2iBody() {

        ilist.addInstruction(getPL0Instruction(InstructionType.RTI, 0,  0));

    }

    /**
     * ... | source level | source address | size
     * Converts array of characters with given descriptor to integer
     * @return instructions list
     */
    public static void s2i() {


        ilist.addInstruction(getPL0Instruction(InstructionType.LIT, 0, 0));

        int jumpAddress = StdLibrary.commandNumber;
        ilist.addInstruction(getPL0Instruction(InstructionType.LIT, 0, 10));
        ilist.addInstruction(getPL0Instruction(InstructionType.OPR, 0, OperationType.MULTIPLY.getHierarchy()));
        ilist.addInstruction(getPL0Instruction(InstructionType.LOD, 0, 3));
        ilist.addInstruction(getPL0Instruction(InstructionType.LOD, 0, 4));
        ilist.addInstruction(getPL0Instruction(InstructionType.PLD, 0, 0));
        ch2iBody();
        ilist.addInstruction(getPL0Instruction(InstructionType.OPR, 0, OperationType.PLUS.getHierarchy()));

        // Increment counters
        incrementCounter(1, 4);
        incrementCounter(-1, 5);

        // check bounds
        checkBounds( 5, jumpAddress, StdLibrary.commandNumber + 5);

    }

    /**
     * Converts string (char array) with given descriptorto float number
     * ... | source level | source address | size
     * CAUTION: point is separator
     */
    public static void s2rBody() {

        ilist.addInstruction(getPL0Instruction(InstructionType.LIT, 0, 0));
        ilist.addInstruction(getPL0Instruction(InstructionType.LIT, 0, 0));

        ilist.addInstruction(getPL0Instruction(InstructionType.JMP, 0, StdLibrary.commandNumber + 6));
        int nextPartAddress = StdLibrary.commandNumber;
        ilist.addInstruction(getPL0Instruction(InstructionType.INT, 0, -1));
        ilist.addInstruction(getPL0Instruction(InstructionType.STO, 0, 6));
        ilist.addInstruction(getPL0Instruction(InstructionType.LIT, 0, 10));
        ilist.addInstruction(getPL0Instruction(InstructionType.OPR, 0, OperationType.DIVIDE.getHierarchy()));
        ilist.addInstruction(getPL0Instruction(InstructionType.LIT, 0, 0));

        int jumpAddress = StdLibrary.commandNumber;
        ilist.addInstruction(getPL0Instruction(InstructionType.LIT, 0, 10));
        ilist.addInstruction(getPL0Instruction(InstructionType.OPR, 0, OperationType.MULTIPLY.getHierarchy()));
        ilist.addInstruction(getPL0Instruction(InstructionType.LOD, 0, 3));
        ilist.addInstruction(getPL0Instruction(InstructionType.LOD, 0, 4));
        ilist.addInstruction(getPL0Instruction(InstructionType.PLD, 0, 0));

        // Increment counters
        incrementCounter(1, 4);
        incrementCounter(-1, 5);

        checkSymbol(8, nextPartAddress, SEPARATOR_CODE);

        ch2iBody();

        ilist.addInstruction(getPL0Instruction(InstructionType.OPR, 0, OperationType.PLUS.getHierarchy()));

        // check bounds
        checkBounds( 5, jumpAddress, StdLibrary.commandNumber + 5);
        ilist.addInstruction(getPL0Instruction(InstructionType.ITR, 0, 0));

    }

    /**
     * Read character and place it on top of the stack
     */
    public static void readCharBody() {

        ilist.addInstruction(getPL0Instruction(InstructionType.REA, 0, 0));

    }

    /**
     * Reads till the end of line or characters of given length
     * | destination level | destination address | size
     */
    public static void readBody() {

        ilist.addInstruction(getPL0Instruction(InstructionType.LIT, 0, 0));

        int jumpAddress = StdLibrary.commandNumber;
        // read
        ilist.addInstruction(getPL0Instruction(InstructionType.REA, 0, 0));

        // Magic number is commands count till end of the program_print_integer read
        checkSymbol(7,StdLibrary.commandNumber + 28, LINE_FEED_CODE);
        checkSymbol(7,StdLibrary.commandNumber + 24, CARRIAGE_RETURN_CODE);

        // save
        ilist.addInstruction(getPL0Instruction(InstructionType.LOD, 0, 3));
        ilist.addInstruction(getPL0Instruction(InstructionType.LOD, 0, 4));
        ilist.addInstruction(getPL0Instruction(InstructionType.PST, 0, 0));

        // Increment counters
        incrementCounter(1, 4);
        incrementCounter(-1, 5);
        // read char count
        incrementCounter(1, 6);

        // check bounds
        checkBounds(5, jumpAddress, StdLibrary.commandNumber + 5);

    }

    /***
     * Loads memory to top of stack
     * ... | source level | source address | destination level | destination address | size
     * Body of memory copy function needs to be wrapped to function
     */
    private static void memCopyBody() {

        int jumpAddress = StdLibrary.commandNumber;

        // load
        ilist.addInstruction(getPL0Instruction(InstructionType.LOD, 0, 3));
        ilist.addInstruction(getPL0Instruction(InstructionType.LOD, 0, 4));
        ilist.addInstruction(getPL0Instruction(InstructionType.PLD, 0, 0));

        // save
        ilist.addInstruction(getPL0Instruction(InstructionType.LOD, 0, 5));
        ilist.addInstruction(getPL0Instruction(InstructionType.LOD, 0, 6));
        ilist.addInstruction(getPL0Instruction(InstructionType.PST, 0, 0));

        // Increment counters
        incrementCounter(1, 4);
        incrementCounter(1, 6);
        incrementCounter(-1, 7);

        // check bounds
        checkBounds( 7, jumpAddress, StdLibrary.commandNumber + 5);

    }

//    // size has to be on top of stack
//    public static void smallocBody() {
//
//        int jumpAddress = StdLibrary.commandNumber;
//        ilist.addInstruction(getPL0Instruction(InstructionType.LOD, 0, -1));
//        ilist.addInstruction(getPL0Instruction(InstructionType.LIT, 0, 1));
//        ilist.addInstruction(getPL0Instruction(InstructionType.OPR, 0, OperationType.MINUS.getHierarchy()));
//        ilist.addInstruction(getPL0Instruction(InstructionType.LOD, 0, -1));
//        ilist.addInstruction(getPL0Instruction(InstructionType.LIT, 0, 1));
//        ilist.addInstruction(getPL0Instruction(InstructionType.OPR, 0, OperationType.GREATER.getHierarchy()));
//        ilist.addInstruction(getPL0Instruction(InstructionType.JMC, 0, StdLibrary.commandNumber + 2));
//        ilist.addInstruction(getPL0Instruction(InstructionType.JMP, 0, jumpAddress));
//
//    }

//    /***
//     * Loads memory to top of stack
//     * ... | level | address | size
//     * Body of memory load function needs to be wrapped
//     * @param commandNumber
//     * @param level
//     * @param stackPosition
//     * @return
//     */
//    private static List<PL0Instruction> memLoad(int commandNumber, int level, int stackPosition) {
//
//        List<PL0Instruction> instructions = new LinkedList<>();
//
//        setParameters(commandNumber, level, stackPosition);
//
//        // Change counter to contain address + length
//        ilist.addInstruction(getPL0Instruction(InstructionType.LOD, 0, 4));
//        ilist.addInstruction(getPL0Instruction(InstructionType.OPR, 0, OperationType.PLUS.getHierarchy()));
//
//        int jumpAddress = StdLibrary.commandNumber;
//
//        // copy
//        ilist.addInstruction(getPL0Instruction(InstructionType.LOD, 0, 3));
//        ilist.addInstruction(getPL0Instruction(InstructionType.LOD, 0, 4));
//        ilist.addInstruction(getPL0Instruction(InstructionType.PLD, 0, 0));
//
//
//        // Increment counter
//        ilist.addInstructionAll(incrementCounter(1, 4));
//
//        // check bounds
//        ilist.addInstructionAll(checkBounds(4, 5, jumpAddress, StdLibrary.commandNumber + 5, OperationType.LESS));
//
//        return instructions;
//    }


//    /***
//     * Save memory to location
//     * ... | level | address | size - 1 | mem[0] | ... | mem[size - 1]
//     * Body of memory save function needs to be wrapped
//     * @param commandNumber
//     * @param level
//     * @param stackPosition
//     * @return
//     */
//    private static List<PL0Instruction> memSave(int commandNumber, int level, int stackPosition) {
//
//        List<PL0Instruction> instructions = new LinkedList<>();
//
//        setParameters(commandNumber, level, stackPosition);
//
//        // Change address to max to save from biggest
//        ilist.addInstruction(getPL0Instruction(InstructionType.LOD, 0, 5));
//        ilist.addInstruction(getPL0Instruction(InstructionType.LOD, 0, 4));
//        ilist.addInstruction(getPL0Instruction(InstructionType.OPR, 0, OperationType.PLUS.getHierarchy()));
//        ilist.addInstruction(getPL0Instruction(InstructionType.STO, 0, 4));
//
//        // Change to min address
//        ilist.addInstruction(getPL0Instruction(InstructionType.LOD, 0, 4));
//        ilist.addInstruction(getPL0Instruction(InstructionType.LOD, 0, 5));
//        ilist.addInstruction(getPL0Instruction(InstructionType.OPR, 0, OperationType.MINUS.getHierarchy()));
//        ilist.addInstruction(getPL0Instruction(InstructionType.STO, 0, 5));
//
//        int jumpAddress = StdLibrary.commandNumber;
//
//        // copy
//        ilist.addInstruction(getPL0Instruction(InstructionType.LOD, 0, 3));
//        ilist.addInstruction(getPL0Instruction(InstructionType.LOD, 0, 4));
//        ilist.addInstruction(getPL0Instruction(InstructionType.PST, 0, 0));
//
//        // decrement counter
//        ilist.addInstructionAll(incrementCounter(-1, 4));
//
//        // check bounds
//        ilist.addInstructionAll(checkBounds(4, 5, jumpAddress, StdLibrary.commandNumber + 5, OperationType.LESS_OR_EQUAL));
//
//        return instructions;
//    }

    private static void incrementCounter(int val, int counterAddress) {

        ilist.addInstruction(getPL0Instruction(InstructionType.LOD, 0, counterAddress));
        ilist.addInstruction(getPL0Instruction(InstructionType.LIT, 0, val));
        ilist.addInstruction(getPL0Instruction(InstructionType.OPR, 0, OperationType.PLUS.getHierarchy()));
        ilist.addInstruction(getPL0Instruction(InstructionType.STO, 0, counterAddress));

    }

    private static void checkBounds(int counterAddress, int passJumpAddress, int failJumpAddress) {

        ilist.addInstruction(getPL0Instruction(InstructionType.LIT, 0, 0));
        ilist.addInstruction(getPL0Instruction(InstructionType.LOD, 0, counterAddress));
        ilist.addInstruction(getPL0Instruction(InstructionType.OPR, 0, OperationType.NOT_EQUALS.getHierarchy()));
        ilist.addInstruction(getPL0Instruction(InstructionType.JMC, 0, failJumpAddress));
        ilist.addInstruction(getPL0Instruction(InstructionType.JMP, 0, passJumpAddress));

    }


    public static void writeCharBody() {

        ilist.addInstruction(getPL0Instruction(InstructionType.WRI, 0, 0));

    }

    /**
     * ... | Source level | Source addres | size
     * Writes characters from character array with given descriptor
     */
    public static void writeBody() {

        int jumpAddress = StdLibrary.commandNumber;

        // load
        ilist.addInstruction(getPL0Instruction(InstructionType.LOD, 0, 3));
        ilist.addInstruction(getPL0Instruction(InstructionType.LOD, 0, 4));
        ilist.addInstruction(getPL0Instruction(InstructionType.PLD, 0, 0));

        // write
        ilist.addInstruction(getPL0Instruction(InstructionType.WRI, 0, 0));

        // Increment counters
        incrementCounter(1, 4);
        incrementCounter(-1, 5);

        // check bounds
        checkBounds(5, jumpAddress, StdLibrary.commandNumber + 5);

        ilist.addInstruction(getPL0Instruction(InstructionType.LIT, 0, CARRIAGE_RETURN_CODE));
        ilist.addInstruction(getPL0Instruction(InstructionType.WRI, 0, 0));
        ilist.addInstruction(getPL0Instruction(InstructionType.LIT, 0, LINE_FEED_CODE));
        ilist.addInstruction(getPL0Instruction(InstructionType.WRI, 0, 0));

    }


    private static void checkSymbol(int position, int jumpAddress, int symbol) {

        ilist.addInstruction(getPL0Instruction(InstructionType.LOD, 0, position));
        ilist.addInstruction(getPL0Instruction(InstructionType.LIT, 0, symbol));
        ilist.addInstruction(getPL0Instruction(InstructionType.OPR, 0 , OperationType.NOT_EQUALS.getHierarchy()));
        ilist.addInstruction(getPL0Instruction(InstructionType.JMC, 0, jumpAddress));

    }

    public static int getCommandNumber() {
        return commandNumber;
    }

    /** Creates map with FunctionDescriptors for every STDlib function
     *
     * @return map of FunctionDescriptors
     */
    public static Map<String, FunctionDescriptor> getStdLibDescriptors() {

        Map<String, FunctionDescriptor> des = new HashMap<>();

        List<Descriptor> arguments = new LinkedList<>();
        arguments.add(new VariableDescriptor(Descriptor.DataType.REAL, null, 0));
        VariableDescriptor vdes = new VariableDescriptor(Descriptor.DataType.INT, null, 0);
        FunctionDescriptor f = new FunctionDescriptor("r2i", vdes.getDataType(), vdes,  arguments, 0);
        f.setStdlib(true);
        des.put("r2i", f);

        arguments = new LinkedList<>();
        arguments.add(new VariableDescriptor(Descriptor.DataType.INT, null, 0));
        vdes = new VariableDescriptor(Descriptor.DataType.CHAR, null, 0);
        f = new FunctionDescriptor("i2ch", vdes.getDataType(), vdes,  arguments, 0);
        f.setStdlib(true);
        des.put("i2ch", f);

        arguments = new LinkedList<>();
        arguments.add(new VariableDescriptor(Descriptor.DataType.INT, null, 0));
        vdes = new VariableDescriptor(Descriptor.DataType.BOOL, null, 0);
        f = new FunctionDescriptor("i2b", vdes.getDataType(), vdes, arguments, 0);
        f.setStdlib(true);
        des.put("i2b", f);

        arguments = new LinkedList<>();
        arguments.add(new VariableDescriptor(Descriptor.DataType.INT, null, 0));
        vdes = new VariableDescriptor(Descriptor.DataType.REAL, null, 0);
        f = new FunctionDescriptor("i2r", vdes.getDataType(), vdes, arguments, 0);
        f.setStdlib(true);
        des.put("i2r", f);

        arguments = new LinkedList<>();
        arguments.add(new VariableDescriptor(Descriptor.DataType.CHAR, null, 0));
        vdes = new VariableDescriptor(Descriptor.DataType.INT, null, 0);
        f = new FunctionDescriptor("ch2i", vdes.getDataType(), vdes, arguments, 0);
        f.setStdlib(true);
        des.put("ch2i", f);

        arguments = new LinkedList<>();
        arguments.add(new VariableDescriptor(Descriptor.DataType.BOOL, null, 0));
        vdes = new VariableDescriptor(Descriptor.DataType.INT, null, 0);
        f = new FunctionDescriptor("b2i", vdes.getDataType(), vdes, arguments, 0);
        f.setStdlib(true);
        des.put("b2i", f);

        arguments = new LinkedList<>();
        arguments.add(new ArrayDescriptor(new VariableDescriptor(Descriptor.DataType.CHAR, null, 0), null, -1, 0));
        arguments.add(new VariableDescriptor(Descriptor.DataType.INT, null, 0));
        vdes = new VariableDescriptor(Descriptor.DataType.INT, null, 0);
        f = new FunctionDescriptor("s2i", vdes.getDataType(), vdes, arguments, 0);
        f.setStdlib(true);
        des.put("s2i", f);

        arguments = new LinkedList<>();
        arguments.add(new ArrayDescriptor(new VariableDescriptor(Descriptor.DataType.CHAR, null, 0), null, -1, 0));
        arguments.add(new VariableDescriptor(Descriptor.DataType.INT, null, 0));
        vdes = new VariableDescriptor(Descriptor.DataType.REAL, null, 0);
        f = new FunctionDescriptor("s2r", vdes.getDataType(), vdes, arguments, 0);
        f.setStdlib(true);
        des.put("s2r", f);

        vdes = new VariableDescriptor(Descriptor.DataType.CHAR, null, 0);
        f = new FunctionDescriptor("readChar", vdes.getDataType(), vdes, 0);
        f.setStdlib(true);
        des.put("readChar", f);

        arguments = new LinkedList<>();
        arguments.add(new ArrayDescriptor(new VariableDescriptor(Descriptor.DataType.CHAR, null, 0), null, -1, 0));
        vdes = new VariableDescriptor(Descriptor.DataType.INT, null, 0);
        f = new FunctionDescriptor("read", vdes.getDataType(), vdes,arguments, 0);
        f.setStdlib(true);
        des.put("read", f);

        arguments = new LinkedList<>();
        arguments.add(new VariableDescriptor(Descriptor.DataType.CHAR, null, 0));
        vdes = new VariableDescriptor(Descriptor.DataType.VOID, null, 0);
        f = new FunctionDescriptor("writeChar", vdes.getDataType(), vdes,arguments, 0);
        f.setStdlib(true);
        des.put("writeChar", f);

        arguments = new LinkedList<>();
        arguments.add(new ArrayDescriptor(new VariableDescriptor(Descriptor.DataType.CHAR, null, 0), null, -1, 0));
        arguments.add(new VariableDescriptor(Descriptor.DataType.INT, null, 0));
        vdes = new VariableDescriptor(Descriptor.DataType.VOID, null, 0);
        f = new FunctionDescriptor("write", vdes.getDataType(), vdes,arguments, 0);
        f.setStdlib(true);
        des.put("write", f);

        return des;

    }

}
