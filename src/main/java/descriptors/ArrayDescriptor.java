package descriptors;

/**
 * Array is just variables placed side by side
 */
public class ArrayDescriptor extends Descriptor {

    private int length;

    // For internal purposes - index access
    private boolean indexed;

    /**
     * Descriptor of elements in array
     */
    private Descriptor itemsDescriptor;

    public ArrayDescriptor(Descriptor des, String name, int length, int level) {
        super(DescriptorType.ARRAY, des.dataType, name, level);
        itemsDescriptor = des.getCopy();

        this.length = length;
        size = length * des.getSize();
        indexed = false;
        itemsDescriptor.setLevel(level);
    }

    public ArrayDescriptor(Descriptor des, String name, int length, int level, boolean constant) {
        super(DescriptorType.ARRAY, des.dataType, name, level, constant);
        itemsDescriptor = des.getCopy();

        this.length = length;
        size = length * des.getSize();
        indexed = false;
        itemsDescriptor.setLevel(level);
    }

    public int getLength() {
        return length;
    }

    public boolean isIndexed() {
        return indexed;
    }

    public void setIndexed(boolean indexed) {
        this.indexed = indexed;
    }

    public Descriptor getItemsDescriptor() {
        return itemsDescriptor;
    }

    public void setItemsDescriptor(Descriptor itemsDescriptor) {
        this.itemsDescriptor = itemsDescriptor;
    }

    @Override
    public void setLevel(int level) {
        this.level = level;
        itemsDescriptor.setLevel(level);
    }

    @Override
    public void setConstant(boolean constant) {
        this.constant = constant;
        itemsDescriptor.setConstant(true);
    }

    @Override
    public Descriptor getCopy() {
        return new ArrayDescriptor(new VariableDescriptor(this.dataType, null, this.level, this.itemsDescriptor.constant), this.name, this.length, this.level, this.constant);
    }

}
