package descriptors;

/**
 * Descriptor class which is extended by specific descriptor types
 */
public class Descriptor {

    public enum DescriptorType { VARIABLE, ARRAY, STRUCT, FUNCTION };
    public enum DataType { INT, REAL, BOOL, CHAR, VOID, STRUCT };

    protected DescriptorType descriptorType;

    protected DataType dataType;

    protected String name;

    /**
     * Determines wheter the descriptor can be changed after inicialization or not
     */
    protected boolean constant;

    // These variables cam be modified during code generation
    protected int address;
    protected int level;
    protected int size;

    protected int levelFound = 0;

    public Descriptor(Descriptor des) {
        descriptorType = des.descriptorType;
        dataType = des.dataType;
        name = des.name;
        constant = des.constant;

        address = des.address;
        size = des.size;
        level = des.level;
    }

    public Descriptor(DescriptorType descriptorType, DataType dataType, String name, int level) {

        this.descriptorType = descriptorType;
        this.dataType = dataType;
        this.name = name;
        this.constant = false;

        address = size = 0;
        this.level = level;
    }

    public Descriptor(DescriptorType descriptorType, DataType dataType, String name, int level, boolean constant) {

        this.descriptorType = descriptorType;
        this.dataType = dataType;
        this.name = name;
        this.constant = constant;

        address = size = 0;
        this.level = level;
    }

    public DescriptorType getDescriptorType() {
        return descriptorType;
    }

    public DataType getDataType() {
        return dataType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAddress() {
        return address;
    }

    public int getLevel() {
        return level;
    }

    public int getSize() {
        return size;
    }

    public void setAddress(int address) {
        this.address = address;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public boolean isConstant() { return constant; }

    public void setConstant(boolean isConstant) {
        constant = isConstant;
    }

    public int getLevelFound() {return levelFound;}

    public void setLevelFound(int level_found) {this.levelFound = level_found;}

    public int getDataTypeSize(DataType dt) {
        switch (dt) {
            case INT:
            case BOOL:
            case CHAR:
                return 1;
            case REAL: return 2;
            default: return 0;
        }
    }

    public Descriptor getCopy() {
        return new Descriptor(this);
    }
}
