package descriptors;

import java.util.LinkedList;
import java.util.List;

public class FunctionDescriptor extends Descriptor {

    /**
     * Function arguments descriptors
     */
    private List<Descriptor> arguments;

    private Descriptor functionReturn;

    // True if function is called in program
    private boolean used;
    // Determines whether function is from std lib or not
    private boolean stdlib;

    public FunctionDescriptor(String name,  DataType dataType, Descriptor functionReturn, int level) {
        super(DescriptorType.FUNCTION, dataType, name, level);
        arguments = new LinkedList<>();
        this.functionReturn = functionReturn.getCopy();
        size = countSize();
        used = false;
        stdlib = false;
    }

    public FunctionDescriptor(String name, DataType dataType, Descriptor functionReturn, List<Descriptor> arguments, int level) {
        super(DescriptorType.FUNCTION, dataType, name, level);
        this.arguments = new LinkedList<Descriptor>(arguments);
        this.functionReturn = functionReturn.getCopy();
        size = countSize();
        used = false;
        stdlib = false;
    }

    public int getArgumentsCount() {
        return arguments.size();
    }

    public Descriptor getFunctionReturn() {
        return functionReturn;
    }

    public Descriptor getArgumentType(int i)  {
        if (arguments.size() < i) {
            return arguments.get(i);
        }
        else {
            throw new IllegalArgumentException(String.format("Function has only %d arguments", arguments.size()));
        }
    }

    public List<Descriptor> getArguments() {
        return arguments;
    }

    public void addArgument(Descriptor argument) {
        arguments.add(argument);
    }

    private int countSize() {
        int sz = 0;
        for (Descriptor des : arguments) {
            sz += des.getSize();
        }
        return sz;
    }

    @Override
    public FunctionDescriptor getCopy() {
        List<Descriptor> args = new LinkedList<>();
        for (Descriptor des : this.arguments) {
            args.add(des.getCopy());
        }

        FunctionDescriptor fd = new FunctionDescriptor(this.name, this.dataType,  this.functionReturn, args, this.level);
        fd.setUsed(this.used);
        fd.setStdlib(this.stdlib);
        return fd;
    }

    public boolean isUsed() {
        return used;
    }

    public void setUsed(boolean used) {
        this.used = used;
    }

    public boolean isStdlib() {return stdlib;}

    public void setStdlib(boolean stdlib) {
        this.stdlib = stdlib;
    }
}
