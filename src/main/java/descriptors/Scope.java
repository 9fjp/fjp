package descriptors;

import semantic.SemanticChecker;

import java.util.HashMap;
import java.util.Map;

/**
 * Class holding all necessary information about scope
 * @param <T> Descriptor type
 */
public class Scope<T extends Descriptor> {

    // descriptors saved with key which is descriptor name
    public Map<String, T> descriptors;
    // if scope is global set to null
    public Scope superior;
    // If scope is child of function scope
    public FunctionDescriptor owner;
    // 0 - global scope, 1 - otherwise
    // because of function declaration only in global scope
    public int level;

    public Scope(Scope superior, int level) {
        this.superior = superior;
        descriptors = new HashMap<String, T>();
        setLevel(level);
        this.owner = SemanticChecker.getCurrentFunctionDescriptor();
    }

    public Scope(Scope superior, int level, Map<String, T> descriptors) {
        this.superior = superior;
        this.descriptors = new HashMap<>();
        for (Map.Entry<String, T> entry : descriptors.entrySet()) {
            T ent = (T) entry.getValue().getCopy();
            ent.setLevel(level);
            this.descriptors.put(entry.getKey(), ent);
        }
        this.level = level;
    }

    public int getSize(){
        int size = 0;
        for (String key : descriptors.keySet()) {
            Descriptor desc = (Descriptor) descriptors.get(key);
            size += desc.getSize();
        }
        return size;
    }

    public void setLevel(int level) {
        this.level = level;
        for (Map.Entry<String, T> entry : descriptors.entrySet()) {
            entry.getValue().setLevel(level);
        }
    }

}
