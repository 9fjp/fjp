package descriptors;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Struct is just members variables or arrays placed side by side
 */
public class StructDescriptor extends Descriptor {

    // Contains all inner members, which are encapsulated in this scope
    private Scope scope;

    // Type of struct - all types are stored in struct definitions table
    private String structureType;

    private List<String> sequence = new LinkedList<>();

    public StructDescriptor(StructDescriptor sd, String name, int level, boolean constant) {
        super(sd);
        this.name = name;
        scope = new Scope<>(sd.scope, level, sd.scope.descriptors);
        this.sequence = new LinkedList<>(sd.sequence);
        structureType = sd.structureType;
        this.constant = constant;
        size = countSize();
        this.level = level;
    }

    public StructDescriptor(String name, String structureType, int level) {
        super(DescriptorType.STRUCT, DataType.STRUCT, name, level);
        scope = new Scope<>(null, level);
        this.structureType = structureType;
        size = countSize();
        this.level = level;
    }

    /**
     * Recomputes address of stack and its members
     * @param address new position of struct
     */
    @Override
    public void setAddress(int address) {

        this.address = address;

        int change = 0;
        for (String name : sequence) {
            Descriptor des = (Descriptor) scope.descriptors.get(name);
            des.setAddress(address + change);
            change += des.size;
        }

    }

    public boolean contains(String internalVariable) {
        return scope.descriptors.containsKey(internalVariable);
    }

    public Descriptor getInternalVariable(String internalVariable) {
        return (Descriptor) scope.descriptors.get(internalVariable);
    }

    public String getStructureType() {
        return structureType;
    }

    private int countSize() {
        int sz = 0;
        for (Object des : scope.descriptors.values()) {
            sz += ((Descriptor)des).getSize();
        }

        return sz;
    }

    public Scope getScope() {
        return scope;
    }

    @Override
    public void setConstant(boolean constant) {
        this.constant = constant;
        for (Object o : scope.descriptors.values()) {
            ((Descriptor) o).setConstant(true);
        }
    }

    @Override
    public Descriptor getCopy() {
        return new StructDescriptor(this, this.name, this.level, this.constant);
    }

    @Override
    public void setLevel(int level) {
        this.level = level;
        scope.setLevel(level);
    }

    public void addInternal(Descriptor des) {
        sequence.add(des.getName());
        scope.descriptors.put(des.getName(), des);
    }
}
