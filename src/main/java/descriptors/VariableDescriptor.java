package descriptors;

public class VariableDescriptor extends Descriptor {

    public VariableDescriptor(DataType dataType, String name, int level) {

        super(DescriptorType.VARIABLE, dataType, name, level);
        size = getDataTypeSize(dataType);
    }

    public VariableDescriptor(DataType dataType, String name, int level, boolean constant) {

        super(DescriptorType.VARIABLE, dataType, name, level, constant);
        size = getDataTypeSize(dataType);
    }

    public VariableDescriptor(DataType dataType, String name, int level, int address) {

        super(DescriptorType.VARIABLE, dataType, name, level);
        size = getDataTypeSize(dataType);
        this.address = address;
    }

    @Override
    public Descriptor getCopy() {
        return new VariableDescriptor(this.dataType, this.name, this.level, constant);
    }


}
