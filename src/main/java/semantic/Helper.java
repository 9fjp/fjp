package semantic;

import compiler.CodeGeneratingVisitor;
import descriptors.*;
import generated.SlangParser;
import org.antlr.v4.runtime.Token;

import java.util.List;

/**
 * Helper class that implements some useful common functions
 */
public class Helper {

    // Determine where access points
    public static Descriptor processAccess(SlangParser.AccessContext context, SemanticChecker sc) {

        Descriptor des = searchSymbolTableScope(sc, context.IDENTIFIER().getText());
        return processAccess(context, des, sc);

    }

    public static Descriptor processAccess(SlangParser.AccessContext context, Descriptor des, SemanticChecker sc) {

        if (des == null) {
            SemanticErrorManager.getInstance().handleError(context.start, "exist");
        }

        if (context.expression() != null) {

            Descriptor edes = sc.visitExpression(context.expression());
            if (edes.getDescriptorType() != Descriptor.DescriptorType.VARIABLE || edes.getDataType() != Descriptor.DataType.INT) {
                SemanticErrorManager.getInstance().handleError(context.start, "invalid_array_index");
            }

            if (des instanceof ArrayDescriptor) {
                des = ((ArrayDescriptor) des).getItemsDescriptor();
            }
            else {
                SemanticErrorManager.getInstance().handleError(context.start, "not_array");
            }
        }

        if (context.access() != null) {
            if (des.getDescriptorType() != Descriptor.DescriptorType.STRUCT) {
                SemanticErrorManager.getInstance().handleError(context.start, "not_struct");
            }

            des =((StructDescriptor) des).getInternalVariable(context.access().IDENTIFIER().getText());
            des = processAccess(context.access(), des, sc);
        }

        return des;

    }

    public static void newScope(SemanticChecker sc, boolean isCurrentSuperior) {

        //Global scope
        Scope symbolSuperiorScope = null;
        Scope structuresDefinitionsSuperiorScope = null;
        int level = 0;
        if (sc.getSymbolTable().size() > 0) {
            if (isCurrentSuperior) {
                symbolSuperiorScope = sc.getCurrentSymbolTableScope();
                structuresDefinitionsSuperiorScope = sc.getCurrentStructuresDefinitionsScope();
                level = 1;
            }
            else {
                symbolSuperiorScope = sc.getSymbolTable().get(0);
                structuresDefinitionsSuperiorScope = sc.getStructuresDefinitions().get(0);
                level = 1;
            }
        }

        sc.setCurrentSymbolTableScope(new Scope<>(symbolSuperiorScope, level));
        sc.getSymbolTable().add(sc.getCurrentSymbolTableScope());
        sc.setCurrentStructuresDefinitionsScope(new Scope<StructDescriptor>(structuresDefinitionsSuperiorScope, level));
        sc.getStructuresDefinitions().add(sc.getCurrentStructuresDefinitionsScope());

    }

    public static void endScope(SemanticChecker sc) {
        sc.setCurrentSymbolTableScope(sc.getCurrentSymbolTableScope().superior);
        sc.setCurrentStructuresDefinitionsScope(sc.getCurrentStructuresDefinitionsScope().superior);
    }

    public static StructDescriptor searchStructuresDefinitionsScope(SemanticChecker sc, String ident) {
        return (StructDescriptor) searchScope(sc.getCurrentStructuresDefinitionsScope(), ident);
    }

    public static StructDescriptor searchStructuresDefinitionsScope(CodeGeneratingVisitor gv, String ident) {
        return (StructDescriptor) searchScope(gv.getCurrentStructuresDefinitionsScope(), ident);
    }

    public static Descriptor searchSymbolTableScope(SemanticChecker sc, String ident) {
        return searchScope(sc.getCurrentSymbolTableScope(), ident);
    }

    public static Descriptor searchSymbolTableScope(CodeGeneratingVisitor gv, String ident) {
        return searchScope(gv.getCurrentScope(), ident);
    }

    private static <T extends Descriptor> T searchScope(Scope<T> scope, String ident) {
        int level_found = 0;
        Scope<T> currentScope = scope;
        T des = currentScope.descriptors.get(ident);
        while (des == null && currentScope.superior != null) {
            currentScope = currentScope.superior;
            des = currentScope.descriptors.get(ident);
        }
        // determine if find in global scope
        if(des != null) {
            if(currentScope.superior == null){
                level_found = 1;
            }
            des.setLevelFound(level_found);
        }
        return des;
    }

    public static void addArguments(List<Descriptor> arguments, SemanticChecker sc) {

        for (Descriptor des : arguments) {
            sc.getCurrentSymbolTableScope().descriptors.put(des.getName(), des);
        }

    }

    public static void addSymbol(SemanticChecker sc, Descriptor des, Token token) {

        if (sc.getCurrentSymbolTableScope().descriptors.containsKey(des.getName())) {
            SemanticErrorManager.getInstance().handleError(token, "defined");
        }
        else {
            if (searchSymbolTableScope(sc, des.getName()) == null) {
                sc.getCurrentSymbolTableScope().descriptors.put(des.getName(), des);
            }
            else {
                SemanticErrorManager.getInstance().handleWarning(token, "hide");
                sc.getCurrentSymbolTableScope().descriptors.put(des.getName(), des);
            }
        }

    }

    public static void addStructDefinition(SemanticChecker sc, StructDescriptor sdes, Token token) {

        if (sc.getCurrentStructuresDefinitionsScope().descriptors.containsKey(sdes.getName())) {
            SemanticErrorManager.getInstance().handleError(token, "defined");
        }
        else {
            if (searchStructuresDefinitionsScope(sc, sdes.getName()) == null) {
                sc.getCurrentStructuresDefinitionsScope().descriptors.put(sdes.getName(), sdes);
            }
            else {
                SemanticErrorManager.getInstance().handleWarning(token, "struct_hide");
            }
        }

    }


}
