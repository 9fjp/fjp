package semantic;

import compiler.StdLibrary;
import descriptors.*;

import java.lang.reflect.Array;
import java.util.*;

import generated.*;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.TerminalNode;

public class SemanticChecker extends SlangBaseVisitor {

    private static FunctionDescriptor currentFunctionDescriptor = null;

    private List<Scope<Descriptor>> symbolTable = new LinkedList<>();
    private List<Scope<StructDescriptor>> structuresDefinitions = new LinkedList<>();

    private Scope<Descriptor> currentSymbolTableScope;
    private Scope<StructDescriptor> currentStructuresDefinitionsScope;

    public static FunctionDescriptor getCurrentFunctionDescriptor() {
        return currentFunctionDescriptor;
    }

    public List<Scope<Descriptor>> getSymbolTable() {
        return symbolTable;
    }

    public List<Scope<StructDescriptor>> getStructuresDefinitions() {
        return structuresDefinitions;
    }

    public Scope<Descriptor> getCurrentSymbolTableScope() {
        return currentSymbolTableScope;
    }

    public Scope<StructDescriptor> getCurrentStructuresDefinitionsScope() {
        return currentStructuresDefinitionsScope;
    }

    public void setCurrentSymbolTableScope(Scope<Descriptor> scope) {
        currentSymbolTableScope = scope;
    }

    public void setCurrentStructuresDefinitionsScope(Scope<StructDescriptor> scope) {
        currentStructuresDefinitionsScope = scope;
    }

    public SemanticChecker() {
        super();
        Helper.newScope(this, false);
    }

    @Override
    public Object visitTranslationUnit(SlangParser.TranslationUnitContext context) {

        if (context.variables() != null) {
            visitVariables(context.variables());
        }

        // Add all stdlib functions descriptors to global scope
        currentSymbolTableScope.descriptors.putAll(StdLibrary.getStdLibDescriptors());

        if (context.functions() != null) {
            visitFunctions(context.functions());
        }

        visitMainFunction(context.mainFunction());

        return null;
    }

    @Override
    public Object visitVariable(SlangParser.VariableContext context) {

        Descriptor des;
        if (context.structDefinition() != null) {
            des = visitStructDefinition(context.structDefinition());
            Helper.addStructDefinition(this, (StructDescriptor) des, context.start);
        }
        else if (context.variableDefinition() != null){
            des = visitVariableDefinition(context.variableDefinition());
            Helper.addSymbol(this, des, context.start);
        }
        else {
            des = visitArrayDefinition(context.arrayDefinition());
            Helper.addSymbol(this, des, context.start);
        }

        if (context.CONST() != null) {
            des.setConstant(true);
        }

        return null;
    }


    @Override
    public Descriptor visitVariableDefinition(SlangParser.VariableDefinitionContext context) {

        String ident = context.IDENTIFIER().getText();
        if (context.expression() != null) {
            Descriptor vdes = visitType(context.type());
            Descriptor edes = visitExpression(context.expression());

            compatibilityCheck(vdes, edes, context, false);

            Descriptor des;
            if (edes.getDescriptorType() == Descriptor.DescriptorType.FUNCTION) {
                des = ((FunctionDescriptor) edes).getFunctionReturn().getCopy();
            }
            else {
                des = edes.getCopy();
            }
            des.setName(ident);

            return des;

        }
        else {
            Descriptor des = visitType(context.type());
            if (des == null) {
                SemanticErrorManager.getInstance().handleError(context.start, "struct_existence");
            }

            if (des.getDataType() == Descriptor.DataType.STRUCT) {
                StructDescriptor sdes = Helper.searchStructuresDefinitionsScope(this, context.type().IDENTIFIER().getText());

                if (sdes == null) {
                    SemanticErrorManager.getInstance().handleError(context.start, "struct_existence");
                }

                return new StructDescriptor(sdes, ident, currentSymbolTableScope.level, false);
            }
            else {
                return new VariableDescriptor(des.getDataType(), ident, currentSymbolTableScope.level);
            }
        }
    }

    @Override
    public Descriptor visitArrayDefinition(SlangParser.ArrayDefinitionContext context) {

        String ident = context.IDENTIFIER().getText();

        if (context.arrayInitializer() != null) {
            Descriptor des = visitType(context.type());
            Descriptor ades = visitArrayInitializer(context.arrayInitializer());

            if (des.getDataType() != ades.getDataType()) {
                SemanticErrorManager.getInstance().handleError(context.start, "match");
            }

            return new ArrayDescriptor(des, ident, context.arrayInitializer().expression().size(), currentSymbolTableScope.level);
        }
        else if (context.NUMBER() != null){
            Descriptor des = visitType(context.type());
            return new ArrayDescriptor(des, ident, Integer.parseInt(context.NUMBER().getText()), currentSymbolTableScope.level);
        }
        else {
            // length - 2 because of "" characters
            return new ArrayDescriptor(new VariableDescriptor(Descriptor.DataType.CHAR, null, currentSymbolTableScope.level), ident, context.STRING_LITERAL().getText().length() - 2, currentSymbolTableScope.level);
        }
    }

    @Override
    public StructDescriptor visitStructDefinition(SlangParser.StructDefinitionContext context) {

        String ident = context.IDENTIFIER().getText();
        StructDescriptor sdes = new StructDescriptor(ident, ident, currentSymbolTableScope.level);
        for (SlangParser.StructInternalVariablesContext cnt : context.structInternalVariables()) {
            Descriptor des = visitStructInternalVariables(cnt);
            sdes.addInternal(des);
        }


        return sdes;
    }

    @Override
    public Descriptor visitStructInternalVariables(SlangParser.StructInternalVariablesContext context) {
        Descriptor des;
        if (context.variableDefinition() != null){
            des = visitVariableDefinition(context.variableDefinition());
        }
        else {
            des = visitArrayDefinition(context.arrayDefinition());
        }

        if (context.CONST() != null) {
            des.setConstant(true);
        }

        return des;
    }


    @Override
    public Descriptor visitAssignment(SlangParser.AssignmentContext context) {

        String ident = context.access().IDENTIFIER().getText();
        Descriptor des = Helper.searchSymbolTableScope(this, ident);
        if (des != null && des.isConstant()) {
            SemanticErrorManager.getInstance().handleError(context.start, "const");
        }

        Descriptor ddes = Helper.processAccess(context.access(), this);
        if (ddes.isConstant()) {
            SemanticErrorManager.getInstance().handleError(context.start, "const");
        }

        Descriptor sdes = visitExpression(context.expression());
        compatibilityCheck(ddes, sdes, context, false);

        return des;
    }

    private void compatibilityCheck(Descriptor ddes, Descriptor sdes, ParserRuleContext context, boolean isStdLib) {

        if (ddes.getDataType() != sdes.getDataType()) {
            SemanticErrorManager.getInstance().handleError(context.start, "match");
        }

        // All has to be checked again if it is function call
        if (ddes.getDescriptorType() != sdes.getDescriptorType()) {
            if (sdes.getDescriptorType() != Descriptor.DescriptorType.FUNCTION) {
                SemanticErrorManager.getInstance().handleError(context.start, "match");
            }
            else if(((FunctionDescriptor) sdes).getFunctionReturn().getDescriptorType() == ddes.getDescriptorType()) {
                Descriptor temp = ((FunctionDescriptor) sdes).getFunctionReturn();
                // stdlib can be called with different lengths
                if (temp.getDescriptorType() == Descriptor.DescriptorType.ARRAY  && ((FunctionDescriptor) sdes).isStdlib() == false) {
                    if (((ArrayDescriptor) temp).getLength() != ((ArrayDescriptor) ddes).getLength()) {
                        SemanticErrorManager.getInstance().handleError(context.start, "length");
                    }
                }
                else if (temp.getDescriptorType() == Descriptor.DescriptorType.STRUCT ) {
                    if (((StructDescriptor) temp).getStructureType() != ((StructDescriptor) ddes).getStructureType()) {
                        SemanticErrorManager.getInstance().handleError(context.start, "match");
                    }
                }

            }
            else {
                SemanticErrorManager.getInstance().handleError(context.start, "match");
            }
        }
        else {
            // datatypes match and descriptors match check stuct type and array length
            // stdlib can be called with different lengths
            if (ddes.getDescriptorType() == Descriptor.DescriptorType.ARRAY && isStdLib == false) {
                if (((ArrayDescriptor) ddes).getLength() != ((ArrayDescriptor) sdes).getLength()) {
                    SemanticErrorManager.getInstance().handleError(context.start, "length");
                }
            }
            else if (ddes.getDescriptorType() == Descriptor.DescriptorType.STRUCT ) {
                if (((StructDescriptor) ddes).getStructureType() != ((StructDescriptor) sdes).getStructureType()) {
                    SemanticErrorManager.getInstance().handleError(context.start, "match");
                }
            }
        }


    }


    @Override
    public Descriptor visitArrayInitializer(SlangParser.ArrayInitializerContext context) {

        Descriptor des;
        if (context.getChildCount() == 1) {
            des = visitExpression(context.expression(0));
        }
        else {
            des = visitExpression(context.expression(0));

            for (int i = 1; i < context.expression().size(); i++) {
                Descriptor edes = visitExpression(context.expression(i));

                if (des.getDataType() != edes.getDataType()) {
                    SemanticErrorManager.getInstance().handleError(context.start, "match");
                }

                if (des.getDescriptorType() != edes.getDescriptorType()) {
                    if (edes.getDescriptorType() != Descriptor.DescriptorType.FUNCTION) {
                        SemanticErrorManager.getInstance().handleError(context.start, "match");
                    }
                    else if (((FunctionDescriptor) edes).getFunctionReturn().getDescriptorType() != des.getDescriptorType()) {
                        SemanticErrorManager.getInstance().handleError(context.start, "match");
                    }
                }
            }
        }

        if (des instanceof ArrayDescriptor) {
            des = ((ArrayDescriptor) des).getItemsDescriptor();
        }
        else if (des instanceof FunctionDescriptor) {
            des = ((FunctionDescriptor) des).getFunctionReturn();
        }

        return des;
    }

    @Override
    public Descriptor visitType(SlangParser.TypeContext context) {

        if (context.STRUCT() != null) {
            return Helper.searchStructuresDefinitionsScope(this, context.IDENTIFIER().getText());
        }
        else {
            return visitSimpleType(context.simpleType());
        }
    }

    @Override
    public Descriptor visitSimpleType(SlangParser.SimpleTypeContext context) {

        if (context.INT() != null) {
            return new VariableDescriptor(Descriptor.DataType.INT, null, currentSymbolTableScope.level);
        }
        else if (context.REAL() != null) {
            return new VariableDescriptor(Descriptor.DataType.REAL, null, currentSymbolTableScope.level);
        }
        else if (context.CHAR() != null) {
            return new VariableDescriptor(Descriptor.DataType.CHAR, null, currentSymbolTableScope.level);
        }
        else {
            return new VariableDescriptor(Descriptor.DataType.BOOL, null, currentSymbolTableScope.level);
        }

    }

    @Override
    public Descriptor visitSimpleExpression(SlangParser.SimpleExpressionContext context) {

        if (context.NUMBER() != null) {
            return new VariableDescriptor(Descriptor.DataType.INT, null, currentSymbolTableScope.level);
        }
        else if (context.REAL_NUMBER() != null) {
            return new VariableDescriptor(Descriptor.DataType.REAL, null, currentSymbolTableScope.level);
        }
        else if (context.FALSE() != null || context.TRUE() != null) {
            return new VariableDescriptor(Descriptor.DataType.BOOL, null, currentSymbolTableScope.level);
        }
        else if(context.CHARACTER() != null) {
            return new VariableDescriptor(Descriptor.DataType.CHAR, null, currentSymbolTableScope.level);
        }
        else if (context.functionCall() != null) {
            return visitFunctionCall(context.functionCall());
        }
        else if (context.access() != null){
            return Helper.processAccess(context.access(), this);
        }
        else {
            return visitExpression(context.expression());
        }

    }

    @Override
    public Descriptor visitUnaryExpression(SlangParser.UnaryExpressionContext context) {

        Descriptor des = visitSimpleExpression(context.simpleExpression());
        Descriptor.DataType dt = des.getDataType();

        if (context.unaryOperator() != null) {

            if (des.getDescriptorType() != Descriptor.DescriptorType.VARIABLE) {
                if (des.getDescriptorType() != Descriptor.DescriptorType.FUNCTION) {
                    SemanticErrorManager.getInstance().handleError(context.start, "operands");
                }
            }

            if (context.unaryOperator().NEGATION() != null && Descriptor.DataType.BOOL != dt) {
                SemanticErrorManager.getInstance().handleError(context.start, "negation");
            }

            if (context.unaryOperator().MINUS() != null && (Descriptor.DataType.INT != dt || Descriptor.DataType.REAL != dt)) {
                SemanticErrorManager.getInstance().handleError(context.start, "unary_minus");
            }
        }

        return des;

    }


    @Override
    public Descriptor visitMultiplicativeExpression(SlangParser.MultiplicativeExpressionContext context) {

        Descriptor des = visitUnaryExpression(context.unaryExpression());

        if (context.me() != null && context.me().getChild(0) != null) {

            Descriptor edes = visitUnaryExpression(context.me().unaryExpression());
            List<Descriptor.DataType> invalid = new ArrayList<>();
            invalid.add(Descriptor.DataType.STRUCT);
            checkTypes(invalid, des, edes, context, "compare_type");
            checkOperands(des, edes, context);

            if (des.getDataType() == Descriptor.DataType.REAL  && context.me().MODULO() != null) {
                SemanticErrorManager.getInstance().handleError(context.start, "modulo");
            }

            checkOperands(des, edes, context);
        }

        return des;
    }

    private void checkOperands(Descriptor ldes, Descriptor rdes, ParserRuleContext context) {

        if (ldes.getDataType() != rdes.getDataType()) {
            SemanticErrorManager.getInstance().handleError(context.start, "match");
        }

        if (ldes.getDescriptorType() != Descriptor.DescriptorType.VARIABLE) {
            if (ldes.getDescriptorType() != Descriptor.DescriptorType.FUNCTION) {
                SemanticErrorManager.getInstance().handleError(context.start, "operands");
            }
            else if (((FunctionDescriptor)ldes).getFunctionReturn().getDescriptorType() != Descriptor.DescriptorType.VARIABLE){
                SemanticErrorManager.getInstance().handleError(context.start, "operands");
            }
        }

        if (rdes.getDescriptorType() != Descriptor.DescriptorType.VARIABLE) {
            if (rdes.getDescriptorType() != Descriptor.DescriptorType.FUNCTION) {
                SemanticErrorManager.getInstance().handleError(context.start, "operands");
            }
            else if (((FunctionDescriptor)rdes).getFunctionReturn().getDescriptorType() != Descriptor.DescriptorType.VARIABLE){
                SemanticErrorManager.getInstance().handleError(context.start, "operands");
            }
        }
    }

    @Override
    public Descriptor visitAdditiveExpression(SlangParser.AdditiveExpressionContext context) {

        Descriptor des = visitMultiplicativeExpression(context.multiplicativeExpression());

        if (context.ae() != null && context.ae().getChild(0) != null) {
            Descriptor edes = visitMultiplicativeExpression(context.ae().multiplicativeExpression());

            List<Descriptor.DataType> invalid = new ArrayList<>();
            invalid.add(Descriptor.DataType.STRUCT);
            checkTypes(invalid, des, edes, context, "compare_type");
            checkOperands(des, edes, context);
        }

        return des;
    }

    @Override
    public Descriptor visitRelationalExpression(SlangParser.RelationalExpressionContext context) {

        Descriptor des = visitAdditiveExpression(context.additiveExpression());

        if (context.re() != null && context.re().getChild(0) != null) {
            Descriptor sdes = visitAdditiveExpression(context.re().additiveExpression());

            List<Descriptor.DataType> invalid = new ArrayList<>();
            invalid.add(Descriptor.DataType.BOOL);
            invalid.add(Descriptor.DataType.STRUCT);
            checkTypes(invalid, des, sdes, context, "compare_type");

            checkOperands(des, sdes, context);

            return new VariableDescriptor(Descriptor.DataType.BOOL, null, currentSymbolTableScope.level);

        }

        return des;

    }

    private void checkTypes(List<Descriptor.DataType> invalid, Descriptor ldes, Descriptor rdes, ParserRuleContext context, String error) {
        for(Descriptor.DataType dt : invalid) {
            if (ldes.getDataType() == dt || rdes.getDataType() == dt) {
                SemanticErrorManager.getInstance().handleError(context.start, error);
            }
        }
    }


    @Override
    public Descriptor visitEqualityExpression(SlangParser.EqualityExpressionContext context) {

        Descriptor des = visitRelationalExpression(context.relationalExpression());

        if (context.ee() != null && context.ee().getChild(0) != null) {
            Descriptor edes = visitRelationalExpression(context.ee().relationalExpression());

            List<Descriptor.DataType> invalid = new ArrayList<>();
            invalid.add(Descriptor.DataType.BOOL);
            invalid.add(Descriptor.DataType.STRUCT);
            checkTypes(invalid, des, edes, context, "compare_type");
            checkOperands(des, edes, context);

            return new VariableDescriptor(Descriptor.DataType.BOOL, null, currentSymbolTableScope.level);
        }

        return des;
    }

    @Override
    public Descriptor visitLogicalAndExpression(SlangParser.LogicalAndExpressionContext context) {

        Descriptor des = visitEqualityExpression(context.equalityExpression());

        if (context.lae() != null && context.lae().getChild(0) != null) {

            Descriptor edes = visitEqualityExpression(context.lae().equalityExpression());
            checkOperands(des, edes, context);

            if (des.getDataType() != Descriptor.DataType.BOOL || edes.getDataType() != Descriptor.DataType.BOOL) {
                SemanticErrorManager.getInstance().handleError(context.start, "logical");
            }
            checkOperands(des, edes, context);

            return new VariableDescriptor(Descriptor.DataType.BOOL, null, currentSymbolTableScope.level);

        }

        return des;

    }

    @Override
    public Descriptor visitExpression(SlangParser.ExpressionContext context) {

        Descriptor des = visitLogicalAndExpression(context.logicalAndExpression());

        if (context.e() != null && context.e().getChild(0) != null) {

            Descriptor edes = visitLogicalAndExpression(context.e().logicalAndExpression());

            if (des.getDataType() != Descriptor.DataType.BOOL || edes.getDataType() != Descriptor.DataType.BOOL) {
                SemanticErrorManager.getInstance().handleError(context.start, "logical");
            }
            checkOperands(des, edes, context);

            return new VariableDescriptor(Descriptor.DataType.BOOL, null, currentSymbolTableScope.level);

        }

        return des;

    }

    @Override
    public Object visitFunction(SlangParser.FunctionContext context) {

        Descriptor rdes = visitFunctionReturnValue(context.functionReturnValue());
        List<Descriptor> arguments = visitArguments(context.arguments());

        String ident = context.IDENTIFIER().getText();
        currentFunctionDescriptor = new FunctionDescriptor(ident, rdes.getDataType(), rdes, arguments, currentSymbolTableScope.level);
        Helper.addSymbol(this, currentFunctionDescriptor, context.start);

        // New scope
        Helper.newScope(this, false);
        Helper.addArguments(arguments, this);


        if (context.body() != null) {
            visitBody(context.body());
        }

        if (context.returnStatement() == null) {
            if (rdes.getDataType() != Descriptor.DataType.VOID) {
                SemanticErrorManager.getInstance().handleError(context.start, "return_missing");
            }
        }
        else {
            Descriptor urdes = visitReturnStatement(context.returnStatement());
            // determine return size of array
            if (rdes.getDescriptorType() == Descriptor.DescriptorType.ARRAY) {
                rdes.setSize(urdes.getSize());
            }
            if (rdes.getDataType() !=  urdes.getDataType()) {
                SemanticErrorManager.getInstance().handleError(context.start, "return_type");
            }
            if (rdes.getDescriptorType() != urdes.getDescriptorType()) {
                if (urdes.getDescriptorType() != Descriptor.DescriptorType.FUNCTION) {
                    SemanticErrorManager.getInstance().handleError(context.start, "return_type");
                }
                else if(((FunctionDescriptor) urdes).getFunctionReturn().getDescriptorType() != rdes.getDescriptorType()) {
                    SemanticErrorManager.getInstance().handleError(context.start, "return_type");
                }
            }
        }
        //Ends scope of body - because we need return value from body scope
        if (context.body() != null) {
            Helper.endScope(this);
        }
        //Ends function scope
        Helper.endScope(this);

        return null;
    }

    @Override
    public Descriptor visitFunctionReturnValue(SlangParser.FunctionReturnValueContext context) {
        if (context.type() != null) {
            if (context.getChildCount() == 1) {
                return visitType(context.type());
            }
            else {
                // determine return array length
                return new ArrayDescriptor(visitType(context.type()), null, Integer.parseInt(context.NUMBER().getText()), currentSymbolTableScope.level);
            }
        }
        else {
            return new VariableDescriptor(Descriptor.DataType.VOID, null, currentSymbolTableScope.level);
        }
    }

    @Override
    public Descriptor visitReturnStatement(SlangParser.ReturnStatementContext context) {
        return visitExpression(context.expression());
    }

    @Override
    public List<Descriptor> visitArguments(SlangParser.ArgumentsContext context) {

        List<Descriptor> arguments = new LinkedList<>();

        if (context != null) {
            int identCount = 0;
            for (int i = 0; i < context.getChildCount(); i++) {
                if (context.getChild(i) instanceof SlangParser.TypeContext) {

                    String ident = context.IDENTIFIER(identCount).getText();
                    Descriptor des = visitType(context.type(identCount));

                    if (((TerminalNode) context.getChild(i + 1)).getSymbol().getType() == SlangParser.IDENTIFIER) {
                        if (des.getDataType() == Descriptor.DataType.STRUCT){
                            arguments.add(new StructDescriptor((StructDescriptor) des, ident, currentSymbolTableScope.level, false));
                        }
                        else {
                            arguments.add(new VariableDescriptor(des.getDataType(), ident, currentSymbolTableScope.level));
                        }
                    } else {
                        arguments.add(new ArrayDescriptor(des, ident, -1, currentSymbolTableScope.level));
                    }
                    identCount++;
                }
            }
        }

        return arguments;

    }

    @Override
    public Descriptor visitFunctionCall(SlangParser.FunctionCallContext context) {

        Descriptor des = Helper.searchSymbolTableScope(this, context.IDENTIFIER().getText());
        if (des == null) {
            SemanticErrorManager.getInstance().handleError(context.start, "exist");
        }

        ((FunctionDescriptor) des).setUsed(true);

        if (des.getDescriptorType() != Descriptor.DescriptorType.FUNCTION) {
            SemanticErrorManager.getInstance().handleError(context.start, "not_function");
        }

        int commaCount = 0;
        for (Descriptor argument : ((FunctionDescriptor) des).getArguments()) {

            if (context.expression(commaCount) != null) {
                Descriptor ades = visitExpression(context.expression(commaCount));
                compatibilityCheck(argument, ades, context, ((FunctionDescriptor) des).isStdlib());
            }
            else {
                SemanticErrorManager.getInstance().handleError(context.start, "arguments_count");
            }

            commaCount++;
        }

        if (context.expression(commaCount) != null) {
            SemanticErrorManager.getInstance().handleError(context.start, "arguments_count");
        }

        return des;
    }

    @Override
    public Object visitIfControl(SlangParser.IfControlContext context) {

        for (int i = 0; i < context.expression().size(); i++) {
            Descriptor des = visitExpression(context.expression(i));
            if (des.getDataType() != Descriptor.DataType.BOOL) {
                SemanticErrorManager.getInstance().handleError(context.start, "if");
            }
            checkVariable(des, context);
        }

        return visitChildren(context);

    }

    @Override
    public Object visitSwitchControl(SlangParser.SwitchControlContext context) {

        Descriptor des = visitExpression(context.expression());
        if (!(des.getDataType() == Descriptor.DataType.CHAR || des.getDataType() == Descriptor.DataType.INT)) {
            SemanticErrorManager.getInstance().handleError(context.start, "switch");
        }
        checkVariable(des, context);

        if (des.getDataType() == Descriptor.DataType.INT && context.CHARACTER().size() > 0) {
            SemanticErrorManager.getInstance().handleError(context.start, "switch_case_char");
        }

        else if (des.getDataType() == Descriptor.DataType.CHAR && context.NUMBER().size() > 0) {
            SemanticErrorManager.getInstance().handleError(context.start, "switch_case_number");
        }

        return visitChildren(context);
    }

    @Override
    public Object visitForControl(SlangParser.ForControlContext context) {


        Descriptor des = Helper.processAccess(context.access(), this);
        if (!(des.getDataType() == Descriptor.DataType.INT || des.getDataType() == Descriptor.DataType.REAL)) {
            SemanticErrorManager.getInstance().handleError(context.start, "for_1st");
        }
        checkVariable(des, context);

        des = visitExpression(context.expression(0));
        if (des.getDataType() != Descriptor.DataType.BOOL) {
            SemanticErrorManager.getInstance().handleError(context.start, "for_2nd");
        }
        checkVariable(des, context);

        des = visitExpression(context.expression(1));
        if (!(des.getDataType() == Descriptor.DataType.INT || des.getDataType() == Descriptor.DataType.REAL)) {
            SemanticErrorManager.getInstance().handleError(context.start, "for_3rd");
        }
        checkVariable(des, context);

        return visitChildren(context);

    }

    @Override
    public Object visitWhileControl(SlangParser.WhileControlContext context) {

        Descriptor des = visitExpression(context.expression());
        if (des.getDataType() != Descriptor.DataType.BOOL) {
            SemanticErrorManager.getInstance().handleError(context.start, "control_condition");
        }
        checkVariable(des, context);

        return visitChildren(context);

    }

    @Override
    public Object visitMainFunction(SlangParser.MainFunctionContext context) {

        currentFunctionDescriptor = new FunctionDescriptor(context.MAIN().getText(), Descriptor.DataType.VOID, new VariableDescriptor(Descriptor.DataType.VOID, null, 0), new LinkedList<Descriptor>(), currentSymbolTableScope.level);
        Helper.addSymbol(this,currentFunctionDescriptor, context.start);

        // New scope
        Helper.newScope(this, false);


        if (context.body() != null) {
            visitBody(context.body());
        }

        Helper.endScope(this);

        return null;
    }

    @Override
    public Object visitBody(SlangParser.BodyContext context) {

        Helper.newScope(this, true);
        visitChildren(context);
        // Exception from rule because return statement
        if (context.parent.getRuleContext() instanceof SlangParser.FunctionContext == false) {
            Helper.endScope(this);
        }

        return null;
    }

    @Override
    public Object visitDoWhileControl(SlangParser.DoWhileControlContext context) {

        Descriptor des = visitExpression(context.expression());
        if ( des.getDataType() != Descriptor.DataType.BOOL) {
            SemanticErrorManager.getInstance().handleError(context.start, "control_condition");
        }
        checkVariable(des, context);

        return visitChildren(context);

    }

    @Override
    public Object visitRepeatUntilControl(SlangParser.RepeatUntilControlContext context) {

        Descriptor des = visitExpression(context.expression());
        if ( des.getDataType() != Descriptor.DataType.BOOL) {
            SemanticErrorManager.getInstance().handleError(context.start, "control_condition");
        }
        checkVariable(des, context);

        return visitChildren(context);

    }

    private void checkVariable(Descriptor des, ParserRuleContext context) {
        if (des.getDescriptorType() != Descriptor.DescriptorType.VARIABLE) {
            if (des.getDescriptorType() != Descriptor.DescriptorType.FUNCTION) {
                SemanticErrorManager.getInstance().handleError(context.start, "not_variable");
            }
            else if (((FunctionDescriptor)des).getFunctionReturn().getDescriptorType() != Descriptor.DescriptorType.VARIABLE){
                SemanticErrorManager.getInstance().handleError(context.start, "not_variable");
            }
        }
    }



}
