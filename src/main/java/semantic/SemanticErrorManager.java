package semantic;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.antlr.v4.runtime.Token;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CancellationException;

public class SemanticErrorManager {

    private static final Logger logger = LoggerFactory.getLogger(SemanticErrorManager.class);

    private static final SemanticErrorManager instance = new SemanticErrorManager();

    private static Map<String, String> errorDictionary;
    private static Map<String, String> warningDictionary;

    private SemanticErrorManager() {
        loadDictionary("semantic_errors", "semantic_warnings");
    }

    public static SemanticErrorManager getInstance() {
        return instance;
    }

    private static void loadDictionary(String errorFileName, String warningFileName) {
        InputStream eis = SemanticErrorManager.class.getClassLoader().getResourceAsStream(errorFileName + ".json");
        InputStream wis = SemanticErrorManager.class.getClassLoader().getResourceAsStream(warningFileName + ".json");
        try {
            errorDictionary = new ObjectMapper().readValue(eis, HashMap.class);
            warningDictionary = new ObjectMapper().readValue(wis, HashMap.class);
        }
        catch(Exception e) {
            errorDictionary =  new HashMap<String, String>();
            logger.error(e.getMessage());
        }
    }


    public void handleError(Token token, String errorName) {
        logger.error(String.format("ERROR ON LINE: %d - %s", token.getLine(), errorDictionary.get(errorName)));
        throw new CancellationException();
    }

    public void handleWarning(Token token, String warningName) {
        logger.warn(String.format("WARNING FOR LINE: %d - %s", token.getLine(), warningDictionary.get(warningName)));
    }


}
